BLACK 2018
===

Code for 2018 version of black-nyc.com

Installation
===

- Clone or download this repo.
- Run npm install
- Run 'gulp' to start watching files.
- When you add new images then run 'gulp imageoptimize'

