<!doctype html>
<html class="no-js" lang="en">

    <head>
        <title>BLACK</title>
        <link rel='shortcut icon' href='../favicon.png' type='image/x-icon'/ >
        <link rel="stylesheet" href="../styles/plugin_styles.min.css">
        <link rel="stylesheet" href="../styles/custom_styles.min.css">
        <?php include('../header.php'); ?>
    </head>

    <body>
        
        <div class="black_page">

            <!-- Tickets -->

            <div class="lightbox switchable_section active" id="tickets">
                <div class="container">

                    <div class="logo_header">
                        <p>MATTE Presents</p>
                        <h1 class="tttravels">B<span>l</span>ack</h1>
                        <p class="explore_text">One Night To Explore.<br><span>One Night To Disrupt.</span></p>
                    </div>
                                        
                    <div id="eventbrite-widget-container-42521914190" class="eventbrite_iframe"></div>

                    <script src="https://www.eventbrite.com/static/widgets/eb_widgets.js"></script>

                    <script type="text/javascript">
                        window.EBWidgets.createWidget({
                            // Required
                            widgetType: 'checkout',
                            eventId: '42521914190',
                            iframeContainerId: 'eventbrite-widget-container-42521914190',

                            // Optional
                            iframeContainerHeight: 425
                        });
                    </script>
                
                </div>

            </div>

        </div>

        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <script src="../scripts/plugin_scripts.min.js"></script>
        <script src="../scripts/custom_scripts.min.js"></script>

    </body>
</html>
