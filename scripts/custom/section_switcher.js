/*----------------------------
Easily show/hide sections of a page by clicking links.
----------------------------*/
    
(function ($) {

  $(document).ready(function () {

    "use strict";

    /*----------------------------
    Section Switcher
    ----------------------------*/

    /*--- Save the different section switcher links and switchable sections on the page ---*/

    const sectionSwitchers = document.querySelectorAll('.section_switcher');

    const switchableSections = document.querySelectorAll('.switchable_section');

    const blackPage = document.querySelector('.black_page');
    
    /*--- Show Section ---*/

    function showSection(sectionName) {

        // Hide all switchable sections on the page

        Array.from(switchableSections).forEach((section) => {
            section.classList.remove('active');
        });

        // Show the new section

        const sectionToShow = document.querySelector('#' + sectionName);

        sectionToShow.classList.add('active');

        // Indicate the section we've switched to in the page container class

        blackPage.classList = 'black_page';

        blackPage.classList.add(sectionName);

    }

    /*--- Section link click events ---*/

    Array.from(sectionSwitchers).forEach((sectionSwitcher) => {

      // On click show the respective section.

      sectionSwitcher.addEventListener('click', (e) => {

        // Prevent default

        e.preventDefault();

        // Show Section

        showSection(sectionSwitcher.dataset.sectionToShow);

      });

    });

    /*--- Hash Section Changer ---*/

    function checkHash() {

      const hash = window.location.hash;

      // Allow deep linking to show specific sections on page load

      if(hash) {
        showSection(hash.substring(1));
      }
      
    }

    checkHash();

  });

})(jQuery);