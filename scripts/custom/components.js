(function($) {

	$(document).ready(function() {

	// Call these functions when the html has loaded.

	"use strict"; // Ensure javascript is in strict mode and catches errors.

		/*================================= 
		BROWSER DETECTION
		---
		Eventbrite checkout is broken in older versions of Safari.
		Need to replace the Tickets button with a direct link to Eventbrite.
		=================================*/

		const ticketsButton = document.querySelector('#tickets_button');

		// Check if user is on Safari version less than 11

		if(bowser.name === 'Safari' && Number(bowser.version) < 11) {

			// Disable the normal section switcher functionality that displays the embedded checkout.
			// Set the link to go to the Eventbrite page.

			ticketsButton.classList.remove('section_switcher');

			ticketsButton.setAttribute('href', 'https://www.eventbrite.com/e/black-nyc-2018-tickets-42521914190');

			ticketsButton.setAttribute('target', '_blank');

		}

		/*================================= 
		FITVIDS
		=================================*/

		/*=== Wrap All Iframes with 'video_embed' for responsive videos ===*/

		$('iframe[src*="youtube.com"], iframe[src*="vimeo.com"]').wrap("<div class='video_embed'/>");

		/*=== Target div for fitVids ===*/

		$(".video_embed").fitVids();

	});

})(jQuery);