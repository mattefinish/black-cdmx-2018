(function ($) {

    $(document).ready(function () {

        "use strict";
                
		/*======================== 
		MAILCHIMP LIST SUBSCRIBE
        ---
		Use mailchimp API to subscribe users to the list.
		Different from regular mailchimp form submission in that the user doesn't have to opt in.
		========================*/

        // Set up variables

        let request;

        // Bind to the submit event of our form

        $(".rsvp_form").submit(function (e) {

            e.preventDefault();

            // Abort any pending request

            if (request) {
                request.abort();
            }

            const form = $(this);

            const rsvpMessage = form.closest('.rsvp_container').find('.rsvp_message');

            // Cache the form fields

            const inputs = form.find("input, select, button, textarea");

            // Serialize the data in the form

            const serializedData = form.serialize();

            // Disable the inputs for the duration of the Ajax request.

            inputs.prop("disabled", true);

            // Send the request to our php file that communicates with the Mailchimp API

            request = $.ajax({
                url: "./includes/mailchimp_form_submit.php",
                type: "post",
                data: serializedData
            });

            // Success Callback

            request.done(function (response, textStatus, jqXHR) {

                // Successful submission

                if (response === 'Success') {
                    rsvpMessage.html("<p>Success.</p>").fadeIn(300);
                }

                // User is already subscribed to the list

                else if (response.indexOf('is already a list member') >= 0) {
                    rsvpMessage.html("<p>You have already registered.</p>").fadeIn(300).delay(3000).fadeOut(300);
                }

                // Other error. Unlikely there will be another type of error but just in case.

                else {
                    rsvpMessage.html("<p>Error. Please try again.</p>").fadeIn(300).delay(3000).fadeOut(300);
                }

            });

            // Always Callback

            request.always(function () {

                // Re-enable form inputs

                inputs.prop("disabled", false);

            });

        });

    });

})(jQuery);