<?php

    /*======================== 
    MAILCHIMP LIST SUBSCRIBE
    Subscribe a user to a given Mailchimp List
    Using jQuery AJAX to capture form input data and submit to this PHP function
    ========================*/
    
    // Include Mailchimp PHP wrapper

    include('./MailChimp.php');

    use \DrewM\MailChimp\MailChimp;

    // Config

    include('./config.php');

    // Setup our Mailchimp API instance with the API key

    $MailChimp = new MailChimp($mailchimp_api_key);

    // Save form variables from the AJAX post

    $list_id = $_POST['listID'];
    $fname = $_POST['fname'];
    $lname = $_POST['lname'];
    $email = $_POST['email'];

    $merge_fields = array('FNAME' => $fname, 'LNAME' => $lname);

    // Subscribe the user to our mailchimp list.

    $result = $MailChimp->post('lists/' . $list_id . '/members', array(
        'email_address' => $email,
        'status'        => 'subscribed',
        'merge_fields'  => $merge_fields
    ));

    // Check for Mailchimp API response and output so we can evaluate in our ajax call.

    if($MailChimp->success()) {
        echo 'Success';
    } else {
        echo $MailChimp->getLastError();
    }

?>