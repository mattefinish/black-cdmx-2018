<!doctype html>
<html class="no-js" lang="en">

    <head>
        <title>BLACK | MEXICO CITY</title>
        <link rel='shortcut icon' href='favicon.png' type='image/x-icon'/ >
        <link rel="stylesheet" href="styles/plugin_styles.min.css">
        <link rel="stylesheet" href="styles/custom_styles.min.css?v=1.0.6">
        <?php include('header.php'); ?>
    </head>

    <body>
        
        <div class="black_page">

            <!-- Landing -->

            <section class="switchable_section landing active" id="landing">

                <div class="landing_section_container">

                    <div class="large_container">

                        <!-- Header -->

                        <header class="landing_section_header main_header">
                            <div class="left">
                                <p><a href="https://matteprojects.com" target="_blank">A MATTE PROJECT</a></p>
                            </div>
                            <div class="center">
                                <img src="img/logo_black.png" class="logo" />
                            </div>
                            <div class="right">
                                <ul class="social_icons flush_right">
                                    <?php include('./components/social.php'); ?>
                                </ul>
                            </div>
                        </header>

                    </div>

                    <div class="large_container">

                        <!-- Music + Arts (Mobile) -->

                        <div class="tablet_landscape_show">
                            <p class="light">Music + Arts<br>Project</p>
                        </div>

                        <!-- CDMX -->

                        <div class="cdmx_logo">CDMX</div>

                        <!-- RSVP Link -->

                        <a class="btn section_switcher" data-section-to-show="info" href="#">Enter</a>

                        <!-- Info -->

                        <div class="panel">
                            <h2>31.12.2018</h2>
                            <h3>New Year's Eve</h3>
                        </div>

                        <div class="panel">
                            <p class="light">Line Up Announced<br>+<br>Tickets Available</p>
                        </div>

                        <!-- A MATTE Project (Mobile) -->

                        <h3 class="tablet_landscape_show">
                            <div class="panel margin_bottom">
                                <a href="https://matteprojects.com" target="_blank">A MATTE PROJECT</a>
                            </div>
                        </h3>

                        <!-- Social (Mobile) -->

                        <div class="tablet_landscape_show">
                            <div class="panel margin_bottom">
                                <ul class="social_icons">
                                    <?php include('./components/social.php'); ?>
                                </ul>
                            </div>
                        </div>

                        <!-- Partners -->

                        <div class="fixed_partner_logos">
                            <ul class="partner_logos">
                                <?php include('./components/partner-logos_black.php'); ?>
                            </ul>
                        </div>

                    </div>

                </div>
            
            </section>

            <!-- Info -->

            <div class="lightbox switchable_section" id="info">
                <div class="container">

                    <div class="fixed_social_icons">
                        <ul class="social_icons flush_right">
                            <?php include('./components/social.php'); ?>
                        </ul>
                    </div>

                    <header class="panel margin_bottom">
                        <div class="logo_header">
                            <p>Sicario + MATTE + 8106 + Trafico<br>Present</p>
                            <h1 class="tttravels">B<span>l</span>ack</h1>
                        </div>
                        <h2>Mexico City</h2>
                        <h2>31.12.2018</h2>
                        <h3>New Year's Eve</h3>
                    </header>
                </div>
                <div class="small_container">
                    <div class="panel margin_bottom">
                        <div class="btn-container">
                            <a class="btn white" href="https://www.ticketmaster.com.mx/black-mexico-city-ner-years-eve-mexico-distrito-federal-31-12-2018/event/14005570E6A36E37?artistid=2573212&majorcatid=10001&minorcatid=201" target="_blank">Tickets</a> <a class="btn white" href="https://pulseradio.net/events/view/15098" target="_blank">Tables</a>
                        </div>
                    </div>
                    <div class="panel margin_bottom">
                        <p class="section_header">Music</p>
                        <h2>Jennifer Cardini</h2>
                        <h2>Barnt</h2>
                        <h2>Matias Aguayo</h2>
                        <h2>Sol Ortega</h2>
                        <h2>Rodion + Justine</h2>
                        <h2>Soni Ceron + Dehesa</h2>
                    </div>
                    <div class="panel margin_bottom">
                        <p class="section_header">Art</p>
                        <h2>Disciplina Studios “8 Cylinders”</h2>
                        <h2>Tansen “Dipsa”</h2>
                    </div>
                    <div class="panel margin_bottom">
                        <p class="section_header">About</p>
                        <p>On New Years Eve, the New York phenomenon, <b>BLACK</b> returns to Mexico City at Auditorio Blackberry featuring a diverse international lineup of artists and DJs.</p> 
                        <p>BLACK unifies music, design, and contemporary art in a singular aesthetic. It transforms a space into an orchestration of form, sound, and culture.</p>
                        <p>Inciting, altering, and disrupting, we explore the world between the imagined and unimagined, submerged in the night.</p>
                    </div>
                    <div class="panel margin_bottom">
                        <p class="section_header">Info</p>
                        <p>NEAREST AIRPORT:<br>MEXICO CITY INTERNATIONAL AIRPORT</p>
                        <p>VENUE:<br><a href="http://auditorioblackberry.com/" target="_blank" class="underlined_link">AUDITORIO BLACKBERRY</a></p>
                        <p>RECOMMENDED HOTELS:<br><a href="https://www.condesadf.com/" target="_blank" class="underlined_link">HOTEL CONDESA</a> | <a href="https://www.hotelhabita.com/" target="_blank" class="underlined_link">HABITA</a> | <a href="http://www.hotelcarlota.com/" target="_blank" class="underlined_link">HOTEL CARLOTA</a></p>
                        <p>OUR MEXICO CITY GUIDE:<br><a href="https://matteprojects.com/city-guides-black-cdmx-puerto-escondido/" target="_blank" class="underlined_link">MORE INFO HERE</a></p>
                    </div>
                    <div class="panel margin_bottom">
                        <p class="matte_project_logo"><a href="http://matteprojects.com/" target="_blank"><img src="./img/a_matte_project_logo_white.png" /></a></p>
                    </div>
                    <div class="tablet_landscape_show">
                        <div class="panel margin_bottom">
                            <ul class="social_icons">
                                <?php include('./components/social.php'); ?>
                            </ul>
                        </div>
                    </div>
                    <ul class="partner_logos partner_logos_footer">
                        <?php include('./components/partner-logos_white.php'); ?>
                    </ul>
                </div>
                <div class="close_icon section_switcher" data-section-to-show="landing" href="#"></div>
            </div>

            <!-- RSVP -->

            <div class="lightbox switchable_section" id="rsvp">

                <div class="container">

                    <!-- Header -->

                    <header class="main_header">
                        <div class="left"></div>
                        <div class="center">
                            <img src="img/logo_white.png" class="logo" />
                        </div>
                        <div class="right"></div>
                    </header>

                    <h2>RSVP</h2>
                    <p class="light">Black Mexico City 2018: <span class="mobile_display_block">Presale Tickets</span></p>
                    
                    <div class="rsvp_container">
                                    
                        <form class="rsvp_form">
                            <div class="form_field">
                                <label><h2>First name</h2></label>
                                <input type="text" name="fname" placeholder="" required>
                            </div>
                            <div class="form_field">
                                <label><h2>Last name</h2></label>
                                <input type="text" name="lname" placeholder="" required>
                            </div>
                            <div class="form_field">
                                <label><h2>Email</h2></label>
                                <input type="email" name="email" placeholder="" required>
                            </div>
                            <input type="hidden" name="listID" value="55edeed76e">
                            <div class="form_submit_container">
                                <button type="submit" class="btn white">Submit</button>
                            </div>
                        </form>
                    
                        <div class="rsvp_message"></div>
                    
                    </div>
                                        
                    <div class="close_icon section_switcher" data-section-to-show="landing" href="#"></div>

                    <!-- Date Info (Mobile) -->

                    <div class="tablet_landscape_show">
                        <div class="panel margin_bottom">
                            <h2>31.12.2018</h2>
                            <h3>New Year's Eve</h3>
                        </div>
                    </div>

                    <!-- Social (Mobile) -->

                    <div class="tablet_landscape_show">
                        <div class="panel margin_bottom">
                            <ul class="social_icons">
                                <?php include('./components/social.php'); ?>
                            </ul>
                        </div>
                    </div>

                    <!-- Partners -->

                    <div class="fixed_partner_logos">
                        <ul class="partner_logos">
                            <?php include('./components/partner-logos_white.php'); ?>
                        </ul>
                    </div>
                
                </div>

            </div>

        </div>

        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <script src="scripts/plugin_scripts.min.js"></script>
        <script src="scripts/custom_scripts.min.js?v=1.0.0"></script>

    </body>

</html>
